<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="generator" content="Mobirise v2.9.10, mobirise.com">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Piclover</title>
<link rel="stylesheet"
	href="https://fonts.googleapis.com/css?family=Roboto:700,400&amp;subset=cyrillic,latin,greek,vietnamese">
<link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" href="assets/mobirise/css/style.css">
<link rel="stylesheet" href="assets/mobirise-gallery/style.css">
<link rel="stylesheet" href="assets/mobirise-slider/style.css">
<link rel="stylesheet" href="assets/mobirise/css/mbr-additional.css"
	type="text/css">

<style>
.menu {
	padding-top: 0.3cm;
}

img {
	max-width: 100%;
	height: auto;
}
</style>



</head>
<body>

	<section
		class="mbr-navbar mbr-navbar--freeze mbr-navbar--absolute mbr-navbar--sticky mbr-navbar--auto-collapse"
		id="menu-0">
	<div class="mbr-navbar__section mbr-section">
		<div class="mbr-section__container container">
			<div class="mbr-navbar__container">
				<div
					class="mbr-navbar__column mbr-navbar__column--s mbr-navbar__brand">
					<span class="mbr-navbar__brand-link mbr-brand mbr-brand--inline">

						<span class="mbr-brand__name"><img src="assets/images/shit.png" height="40px" width ="40px"/></span><a
							class="mbr-brand__name text-white" href="#">   &ensp; Computerlover</a></span>
					</span>
				</div>
				<div class="mbr-navbar__hamburger mbr-hamburger">
					<span class="mbr-hamburger__line"></span>
				</div>
				<div class="mbr-navbar__column mbr-navbar__menu">
					<nav
						class="mbr-navbar__menu-box mbr-navbar__menu-box--inline-right">
					<div class="mbr-navbar__column">
						<ul
							class="mbr-navbar__items mbr-navbar__items--right mbr-buttons mbr-buttons--freeze mbr-buttons--right btn-decorator btn-inverse mbr-buttons--active">
							<li class="mbr-navbar__item"><form action="MainPageServlet">
									<button class="mbr-buttons__link btn" type="submit">HOME</button>
								</form></li>
							<li class="mbr-navbar__item"><form
									action="SecretKittenServlet">
									<input type="hidden" name="from" value="SecretItServlet">
									<button class="mbr-buttons__link btn" type="submit"
										name="wheretosecret" value="tokittens">SECRET KITTENS</button>
								</form></li>
							<li class="mbr-navbar__item"><a
								class="mbr-buttons__link btn text-white" href=#>SECRET
									COMPUTERS</a></li>
							<li class="mbr-navbar__item"><form action="about">
									<button class="mbr-buttons__link btn" type="submit">ABOUT</button>
								</form></li>

							<c:if test="${(loggedin == true) || (cookie.loggedin != null)}">
								<li class="mbr-navbar__item"><form action="changeuser">
								<input type="hidden" name="from" value="SecretItServlet">
										<button class="mbr-buttons__btn btn btn-default">Logged
											in as: ${userbean.nickname}</button>
									</form></li>

								<li class="mbr-navbar__item"><form action="logout">

										<button class="mbr-buttons__btn btn btn-default">log
											out</button>
									</form></li>
							</c:if>
						</ul>
					</div>

				</nav>
				</div>
			</div>
		</div>
	</div>
	</section>



	<section class="engine"> </section>
	<section
		class="mbr-gallery mbr-section mbr-section--no-padding mbr-after-navbar"
		id="gallery1-3" style="background-color: rgb(76, 105, 114);">
	<!-- Gallery -->
	<div class=" container mbr-gallery-layout-default">
		<div>
			<div class="row mbr-gallery-row no-gutter">
				<c:forEach var="i" begin="0" end="5">
					<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 mbr-gallery-item">
						<a href="#lb-gallery1-3" data-slide-to="0" data-toggle="modal">
							<img alt="" src="showimage?index=${i}"> <span
							class="icon glyphicon glyphicon-zoom-in"></span>
						</a>
					</div>

				</c:forEach>
			</div>
		</div>
		<div class="clearfix"></div>
	</div>

	<!-- Lightbox -->
	<div data-app-prevent-settings=""
		class="mbr-slider modal fade carousel slide" tabindex="-1"
		data-keyboard="true" data-interval="false" id="lb-gallery1-3">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-body">
					<ol class="carousel-indicators">
						<li data-app-prevent-settings="" data-target="#lb-gallery1-3"
							data-slide-to="0"></li>
						<li data-app-prevent-settings="" data-target="#lb-gallery1-3"
							data-slide-to="1"></li>
						<li data-app-prevent-settings="" data-target="#lb-gallery1-3"
							class=" active" data-slide-to="2"></li>
						<li data-app-prevent-settings="" data-target="#lb-gallery1-3"
							data-slide-to="3"></li>
						<li data-app-prevent-settings="" data-target="#lb-gallery1-3"
							data-slide-to="4"></li>
						<li data-app-prevent-settings="" data-target="#lb-gallery1-3"
							data-slide-to="5"></li>
					</ol>
					<div class="carousel-inner">
						<div class="item">
							<img alt="" src="showimage?index=0">
						</div>
						<div class="item">
							<img alt="" src="showimage?index=1">
						</div>
						<div class="item active">
							<img alt="" src="showimage?index=2">
						</div>
						<div class="item">
							<img alt="" src="showimage?index=3">
						</div>
						<div class="item">
							<img alt="" src="showimage?index=4">
						</div>
						<div class="item">
							<img alt="" src="showimage?index=5">
						</div>
					</div>
					<a class="left carousel-control" role="button" data-slide="prev"
						href="#lb-gallery1-3"> <span
						class="glyphicon glyphicon-menu-left" aria-hidden="true"></span> <span
						class="sr-only">Previous</span>
					</a> <a class="right carousel-control" role="button" data-slide="next"
						href="#lb-gallery1-3"> <span
						class="glyphicon glyphicon-menu-right" aria-hidden="true"></span>
						<span class="sr-only">Next</span>
					</a> <a class="close" href="#" role="button" data-dismiss="modal">
						<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
						<span class="sr-only">Close</span>
					</a>
				</div>
			</div>
		</div>
	</div>
	</section>

	<div class="menu">
		<div class="mbr-section__container container">


			<div class="mbr-section__col col-lg-4 col-xs-12 col-md-4 col-sm-6">

				<div class="mbr-section__container mbr-section__container--last">
					<h3 class="mbr-header__text">COMPUTERS!!</h3>
				</div>
			</div>


			<div class="mbr-section__col  col-lg-4 col-xs-12 col-md-4 col-sm-6">

				<div class="mbr-section__container mbr-section__container--last">
					<div class="mbr-buttons mbr-buttons--center">
						<form method="get">
							<button type="submit" name="kittensbutton" value="tokittens"
								class="mbr-buttons__btn btn btn-wrap btn-xs-lg btn-default">
								SCRAMBLE IMAGES<br>
							</button>
						</form>
					</div>
				</div>
			</div>
		</div>


	</div>

	<footer
		class="mbr-section mbr-section--relative mbr-section--fixed-size"
		id="footer1-2" style="background-color: rgb(68, 68, 68);">

	<div class="mbr-section__container container">
		<div class="mbr-footer mbr-footer--wysiwyg row">
			<div class="col-sm-12">
				<p class="mbr-footer__copyright"></p>
				<p>Copyright (c) 2015 Animal Fauve</p>
				<p></p>
			</div>
		</div>
	</div>
	</footer>


	<script src="assets/web/assets/jquery/jquery.min.js"></script>
	<script src="assets/bootstrap/js/bootstrap.min.js"></script>
	<script src="assets/smooth-scroll/SmoothScroll.js"></script>
	<script src="assets/masonry/masonry.pkgd.min.js"></script>
	<script src="assets/imagesloaded/imagesloaded.pkgd.min.js"></script>
	<script
		src="assets/bootstrap-carousel-swipe/bootstrap-carousel-swipe.js"></script>
	<script src="assets/mobirise/js/script.js"></script>
	<script src="assets/mobirise-gallery/script.js"></script>


</body>
</html>