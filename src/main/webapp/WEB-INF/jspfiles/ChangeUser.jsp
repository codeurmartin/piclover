<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">

<link href="css/bootstrap.min.css" rel="stylesheet">
<link href='https://fonts.googleapis.com/css?family=Droid+Sans:700'
	rel='stylesheet' type='text/css'>
<style>
            #packtpub {
                padding-top: 50px;
                padding-bottom: 25px;
                padding-right: 50px;
                padding-left: 50px;
            }

            .appliedtoall {
                font-family: 'Droid Sans', sans-serif;
            }

            #center {
                text-align: center;
            }

            * {
                border-radius: 0 !important;
            }

            div.maindiv {
                max-width: 550px;
                margin: auto;
                border: 1px solid black;
                padding-top: 20px;
                padding-bottom: 25px;
                padding-right: 50px;
                padding-left: 50px;
            }
        </style>
        <title>Change User</title>
    </head>

    <body class="appliedtoall">

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>

        <script src="assets/javascript/bootstrap.min.js"></script>

        <nav class="navbar navbar-inverse navbar-fixed-top">
            <div class="container-fluid" id="navigationBar">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                        <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#">Piclover</a>
                </div>
                <div class="collapse navbar-collapse" id="myNavbar">
                    <ul class="nav navbar-nav">
                        <li class="active"><a href="/piclover/MainPageServlet">Return
						Home</a></li>
                    </ul>
                </div>
            </div>
        </nav>


        <br />
        <br />
        <br />
        <br />

        <div class="maindiv">
            <form role="form" method="post">

                <div>
                    <h1 id="center">change yourself, ${userbean.nickname}</h1>
                    <h3 id="center">${requestScope.password}</h3>
                </div>

                <div class="form-group">
                    <label> First Name</label>
                    <input type="text" class="form-control" name="firstname" placeholder="give your first name baby" value="${userbean.firstname}" />
                </div>
                <div class="form-group">
                    <label>Last Name</label>
                    <input type="text" class="form-control" name="lastname" placeholder="last name here plzzz" value="${userbean.lastname}" />
                </div>
                <div class="form-group">
                    <label>Street</label>
                    <input type="text" class="form-control" name="street" placeholder="your street yo" value="${userbean.street}" />
                </div>
                <div class="form-group">
                    <label>Postcode</label>
                    <input type="text" class="form-control" name="postcode" placeholder="zipydipy" value="${userbean.postcode}" />
                </div>

                <div class="form-group">
                    <label>Password</label>
                    <input type="password" class="form-control" placeholder="password, atleast 6 chars" name="password" value="" />
                </div>
                <div class="form-group">
                    <label>Retype Password</label>
                    <input type="password" class="form-control" placeholder="retype Son!" name="retypepassword" value="" />
                </div>


                <div class="row">


                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                        <form method="post">
                            <button type="submit" class="btn btn-primary" style="width: 100%">Submit</button>
                        </form>

                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                        <form action="logout">
                            <button type="submit" class="btn btn-primary" style="width: 100%">logout</button>
                        </form>

                    </div>

                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                        <form method="post">
                            <input type="hidden" name="from" value="${param.from}">
                            <button type="submit" style="width: 100%" class="btn btn btn-primary" name="return" value="return">return</button>
                        </form>
                    </div>


                </div>
            </form>
        </div>
    </body>

    </html>