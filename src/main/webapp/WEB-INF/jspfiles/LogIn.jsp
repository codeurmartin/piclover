<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link href="css/bootstrap.min.css" rel="stylesheet">
<link href='https://fonts.googleapis.com/css?family=Droid+Sans:700'
	rel='stylesheet' type='text/css'>

<style>
#packtpub {
	padding-top: 50px;
	padding-bottom: 25px;
	padding-right: 50px;
	padding-left: 50px;
}

.appliedtoall {
	font-family: 'Droid Sans', sans-serif;
}

#center {
	text-align: center;
}

* {
	border-radius: 0 !important;
}

div.maindiv {
	max-width: 450px;
	margin: auto;
	border: 1px solid black;
	padding-top: 20px;
	padding-bottom: 25px;
	padding-right: 50px;
	padding-left: 50px;
}
</style>
<title>Log In</title>
</head>

<body class="appliedtoall">

	<script
		src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>

	<script src="assets/javascript/bootstrap.min.js"></script>

	<nav class="navbar navbar-inverse navbar-fixed-top">
	<div class="container-fluid" id="navigationBar">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse"
				data-target="#myNavbar">
				<span class="icon-bar"></span> <span class="icon-bar"></span> <span
					class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="#">Piclover</a>
		</div>
		<div class="collapse navbar-collapse" id="myNavbar">
			<ul class="nav navbar-nav">
				<li class="active"><a href="/piclover/MainPageServlet">Return
						Home</a></li>
			</ul>
		</div>
	</div>
	</nav>


	<br />
	<br />
	<br />
	<br />

	<div class="maindiv">
		<form role="form" method="post">

			<div>
				<h1 id="center">LOG IN!</h1>
				<h3 id="center">${requestScope.invalidinput}</h3>
			</div>

			<div class="form-group">
				<label> Enter Nickname </label> <input type="text"
					class="form-control" id="enterusername" placeholder="nickname"
					value="${userbean.nickname}" name="nickname" />
			</div>
			<div class="form-group">
				<label for="enterpassword">Password</label> <input type="password"
					class="form-control" id="enterpassword" placeholder="Password"
					name="password" />
			</div>

			<div class="checkbox">
				<label> <input type="checkbox" name="remember"
					value="remember"> Keep me signed in
				</label>
			</div>
			<div class="row">
				<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">

					<button type="submit" class="btn btn-primary" style="width: 100%">Login</button>


				</div>
</form>
				<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
					<form action="signup">
						<input type="hidden" name="from" value="${param.from}">
						<button type="submit" class="btn btn-primary" style="width: 100%">Sign
							up</button>
					</form>
				</div>
				<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
					<form method="post">
						<input type="hidden" name="from" value="${param.from}">
						<button type="submit" style="width: 100%" class="btn btn-primary"
							name="return" value="return">return</button>
				</div>

			</div>

	</div>
</body>



</html>