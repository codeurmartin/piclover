<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">


<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="generator" content="Mobirise v2.9.10, mobirise.com">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Piclover</title>
<link rel="stylesheet"
	href="https://fonts.googleapis.com/css?family=Roboto:700,400&amp;subset=cyrillic,latin,greek,vietnamese">
<link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" href="assets/mobirise/css/style.css">
<link rel="stylesheet" href="assets/mobirise-gallery/style.css">
<link rel="stylesheet" href="assets/mobirise-slider/style.css">
<link rel="stylesheet" href="assets/mobirise/css/mbr-additional.css"
	type="text/css">

<style>
.menu {
	padding-top: 0.3cm;
}

img {
	max-width: 100%;
	height: auto;
}
</style>

<title>About</title>

</head>
<body>




	<section
		class="mbr-navbar mbr-navbar--freeze mbr-navbar--absolute mbr-navbar--sticky mbr-navbar--auto-collapse"
		id="menu-0">
	<div class="mbr-navbar__section mbr-section">
		<div class="mbr-section__container container">
			<div class="mbr-navbar__container">
				<div
					class="mbr-navbar__column mbr-navbar__column--s mbr-navbar__brand">
					<span class="mbr-navbar__brand-link mbr-brand mbr-brand--inline">

						<span class="mbr-brand__name"><img src="assets/images/spookje.jpg" height="40px" width ="40px"/></span><a
							class="mbr-brand__name text-white" href="#">   &ensp; About</a></span>
					</span>
				</div>
				<div class="mbr-navbar__hamburger mbr-hamburger">
					<span class="mbr-hamburger__line"></span>
				</div>
				<div class="mbr-navbar__column mbr-navbar__menu">
					<nav
						class="mbr-navbar__menu-box mbr-navbar__menu-box--inline-right">
					<div class="mbr-navbar__column">
						<ul
							class="mbr-navbar__items mbr-navbar__items--right mbr-buttons mbr-buttons--freeze mbr-buttons--right btn-decorator btn-inverse mbr-buttons--active">
							<li class="mbr-navbar__item"><form action="MainPageServlet">
									<button class="mbr-buttons__link btn" type="submit">HOME</button>
								</form></li>
							<li class="mbr-navbar__item"><form
									action="SecretKittenServlet">
									<input type="hidden" name="from" value="SecretKittenServlet">
									<button class="mbr-buttons__link btn" type="submit">SECRET
										KITTENS</button>
								</form></li>
							<li class="mbr-navbar__item"><form action="SecretItServlet">
									<input type="hidden" name="from" value="SecretItServlet">
									<button class="mbr-buttons__link btn" type="submit">SECRET
										COMPUTERS</button>
								</form></li>
							<li class="mbr-navbar__item"><a
								class="mbr-buttons__link btn text-white" href=#>ABOUT</a></li>

							<c:if test="${(loggedin != true) && ((cookie.loggedin == null))}">
								<li class="mbr-navbar__item"><form action="login">
								<input type="hidden" name="from" value="about">
										<button type="submit" class="mbr-buttons__btn btn btn-default">Log
											in</button>
									</form></li>
								<li class="mbr-navbar__item"><form action="signup">
								<input type="hidden" name="from" value="about">
										<button class="mbr-buttons__btn btn btn-default">Sign
											in</button>
									</form></li>
							</c:if>
							<c:if test="${(loggedin == true) || (cookie.loggedin != null)}">
								<li class="mbr-navbar__item"><form action="changeuser">
										<input type="hidden" name="from" value="about">
										<button class="mbr-buttons__btn btn btn-default">Logged
											in as: ${userbean.nickname}</button>
									</form></li>

								<li class="mbr-navbar__item"><form action="logout">
										<input type="hidden" name="from" value="about">
										<button class="mbr-buttons__btn btn btn-default">log
											out</button>
									</form></li>
							</c:if>
						</ul>
					</div>

					</nav>
				</div>
			</div>
		</div>
	</div>
	</section>

	<c:forEach var="i" begin="1" end="500"> ABOUT!
   </c:forEach>
	<p>


		<footer
			class="mbr-section mbr-section--relative mbr-section--fixed-size"
			id="footer1-2" style="background-color: rgb(68, 68, 68);">

		<div class="mbr-section__container container">
			<div class="mbr-footer mbr-footer--wysiwyg row">
				<div class="col-sm-12">
					<p class="mbr-footer__copyright"></p>
					<p>Copyright (c) 2015 Animal Fauve</p>
					<p></p>
				</div>
			</div>
		</div>
		</footer>


		<script src="assets/web/assets/jquery/jquery.min.js"></script>
		<script src="assets/bootstrap/js/bootstrap.min.js"></script>
		<script src="assets/smooth-scroll/SmoothScroll.js"></script>
		<script src="assets/masonry/masonry.pkgd.min.js"></script>
		<script src="assets/imagesloaded/imagesloaded.pkgd.min.js"></script>
		<script
			src="assets/bootstrap-carousel-swipe/bootstrap-carousel-swipe.js"></script>
		<script src="assets/mobirise/js/script.js"></script>
		<script src="assets/mobirise-gallery/script.js"></script>
</body>
</body>
</html>