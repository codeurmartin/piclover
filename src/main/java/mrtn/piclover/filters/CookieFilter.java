package mrtn.piclover.filters;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;

import mrtn.piclover.beans.UserBean;
import mrtn.piclover.dao.DAOFactory;
import mrtn.piclover.dao.UserDAO;

/*
 * checks if user is logged in with a cookie while the session expired and if so binds the user to the session;
 */
public class CookieFilter implements Filter {

	public CookieFilter() {

	}

	public void destroy() {

	}

	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		HttpServletRequest request1 = (HttpServletRequest) request;

		if (request1.getSession().isNew()) {
			if (request1.getCookies() != null) {
				for (Cookie k : request1.getCookies()) {
					if (k != null) {
						if (k.getName().equals("loggedin")) {
							UserBean user = new UserBean();
							DAOFactory javabase = DAOFactory.getInstance("javabase.jdbc");
							UserDAO userDAO = javabase.getUserDAO();
							user = userDAO.find(k.getValue());
							request1.getSession().setAttribute("userbean", user);
							request1.getSession().setAttribute("loggedin", true);
						}
					}

				}
			}
		}
		chain.doFilter(request, response);
	}

	public void init(FilterConfig fConfig) throws ServletException {

	}

}
