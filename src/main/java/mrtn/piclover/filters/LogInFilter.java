package mrtn.piclover.filters;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebFilter(servletNames = { "login", "signup" })
public class LogInFilter implements Filter {

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		HttpServletRequest request1 = (HttpServletRequest) request;
		HttpServletResponse response1 = (HttpServletResponse) response;

		// checks whether user is logged or not by putting the two logged in
		// "checkers" (the cookie or the session attribute) in an array and then
		// checks whether either one of them is not null
		Object[] loggedIn = new Object[2];
		Cookie cookies[] = request1.getCookies();
		for (Cookie c : cookies) {
			if (c.getName().equals("loggedin")) {
				loggedIn[0] = "true";
			}
		}
		loggedIn[1] = request1.getSession().getAttribute("loggedin");

		if (loggedIn[0] != null || loggedIn[1] != null) {
			if (request1.getSession().getAttribute("from") != null) {
				response1.sendRedirect((String) request1.getSession().getAttribute("from"));
			} else {
				request.getRequestDispatcher("MainPageServlet").forward(request1, response1);
			}

		} else {
			chain.doFilter(request1, response1);
		}

	}

	@Override
	public void destroy() {
		// TODO Auto-generated method stub

	}

}
