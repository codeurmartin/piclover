package mrtn.piclover.filters;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;

@WebFilter(servletNames = { "MainPageServlet", "AboutServlet" })
public class WhereFromFilter implements Filter {

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		// TODO Auto-generated method stub

	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {

		HttpServletRequest request1 = (HttpServletRequest) request;
		request1.getSession().setAttribute("from", request1.getRequestURI());

		String[] wheretoparameters = request.getParameterValues("wheretosecret");
		if (wheretoparameters != null) {
			for (String s : wheretoparameters) {
				if (s.equals("tokittens")) {
					request1.getSession().setAttribute("wheretosecret", "kittens");
				} else if (s.equals("tocomputers")) {
					request1.getSession().setAttribute("wheretosecret", "computers");
				}
			}
		}

		chain.doFilter(request1, response);

	}

	@Override
	public void destroy() {
		// TODO Auto-generated method stub

	}

}
