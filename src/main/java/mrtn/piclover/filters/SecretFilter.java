package mrtn.piclover.filters;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;

/**
 * Servlet Filter implementation class SecretFilter
 */
@WebFilter(servletNames = { "changeuser", "SecretItServlet", "SecretKittenServlet" })

public class SecretFilter implements Filter {

	/**
	 * Default constructor.
	 */
	public SecretFilter() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see Filter#destroy()
	 */
	public void destroy() {
		// TODO Auto-generated method stub
	}

	/**
	 * @see Filter#doFilter(ServletRequest, ServletResponse, FilterChain)
	 */
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		HttpServletRequest request1 = (HttpServletRequest) request;
		Object[] loggedIn = new Object[2];
		Cookie cookies[] = request1.getCookies();
		for (Cookie c : cookies) {
			if (c.getName().equals("loggedin")) {
				loggedIn[0] = "true";
			}
		}
		loggedIn[1] = request1.getSession().getAttribute("loggedin");

		if (loggedIn[0] != null || loggedIn[1] != null) {
			chain.doFilter(request, response);
		} else {
			request.getRequestDispatcher("login").forward(request, response);
		}
	}

	/**
	 * @see Filter#init(FilterConfig)
	 */
	public void init(FilterConfig fConfig) throws ServletException {
		// TODO Auto-generated method stub
	}

}
