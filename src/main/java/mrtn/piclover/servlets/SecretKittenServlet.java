package mrtn.piclover.servlets;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mrtn.piclover.beans.ImageBean;
import mrtn.piclover.services.ImagePathService;

public class SecretKittenServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public SecretKittenServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * Checks first if user is logged in by checking if the session attribute
	 * loggedin has been initialized and by checking if the loggedin cookies has
	 * been initialized; If user is loggedIn the requestDispatcher will forward
	 * the request/response to the secretkittenspage.
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		List<ImageBean> beanList = ImagePathService.getImageBeans("kittens");
		request.getSession().setAttribute("imagebeans", beanList);
		request.getRequestDispatcher("/WEB-INF/jspfiles/SecretKittensPage.jsp").forward(request, response);

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		doGet(request, response);
	}

}
