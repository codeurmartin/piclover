package mrtn.piclover.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mrtn.piclover.beans.UserBean;
import mrtn.piclover.dao.DAOFactory;
import mrtn.piclover.dao.UserDAO;
import mrtn.piclover.services.UserValidator;

/**
 * Servlet implementation class ChangeUserServlet
 */
@WebServlet
public class ChangeUserServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ChangeUserServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		request.getRequestDispatcher("/WEB-INF/jspfiles/ChangeUser.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		if (request.getParameter("return") == null) {
			UserBean user = (UserBean) request.getSession().getAttribute("userbean");
			boolean isValid = UserValidator.validateChangeUser(request, user);

			String password = request.getParameter("password");

			if (password.length() > 0) {
				if (UserValidator.validatePassword(request, user)) {
					request.setAttribute("password", "password succesfully changed");
					DAOFactory javabase = DAOFactory.getInstance("javabase.jdbc");
					UserDAO userDAO = javabase.getUserDAO();
					userDAO.changePassword(user);
				} else {
					request.setAttribute("password", "password failed to change");
				}
			}

			if (isValid) {
				DAOFactory javabase = DAOFactory.getInstance("javabase.jdbc");
				UserDAO userDAO = javabase.getUserDAO();
				userDAO.update(user);
			}
			doGet(request, response);
		} else {
			response.sendRedirect(request.getParameter("from"));
		}

	}

}
