package mrtn.piclover.servlets;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mrtn.piclover.beans.ImageBean;

/**
 * Servlet implementation class ShowImageServlet
 */

/*
 */
public class ShowImageServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final String IMAGEINDEX = "imageindex";

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ShowImageServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		int index = Integer.valueOf(request.getParameter("index"));
		List<ImageBean> beanList = (List<ImageBean>) request.getSession().getAttribute("imagebeans");
		Path path = Paths.get(System.getenv("UPLOAD_LOCATION") + beanList.get(index).getPath());

		byte[] imageBytes = Files.readAllBytes(path);
		response.setContentType("image/jpeg");
		response.setContentLength(imageBytes.length);
		response.getOutputStream().write(imageBytes);

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
