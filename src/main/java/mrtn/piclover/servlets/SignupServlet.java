package mrtn.piclover.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mrtn.piclover.beans.UserBean;
import mrtn.piclover.dao.DAOFactory;
import mrtn.piclover.dao.UserDAO;
import mrtn.piclover.services.UserValidator;

/**
 * Servlet implementation class SignupServlet
 */
@WebServlet
public class SignupServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public SignupServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		Object isValid = request.getAttribute("isValid");
		if (request.getParameter("return") == null) {
			if (request.getAttribute("loggedin") != null) {
				request.getRequestDispatcher("/MainPageServlet").forward(request, response);
			} else {
				if (isValid == null) {
					request.getRequestDispatcher("/WEB-INF/jspfiles/signup.jsp").forward(request, response);
				} else {
					boolean valid = (Boolean) isValid;
					if (valid) {
						request.getRequestDispatcher("/MainPageServlet").forward(request, response);
					} else {
						request.setAttribute("invalidinput", "Something went wrong!");
						request.getRequestDispatcher("/WEB-INF/jspfiles/signup.jsp").forward(request, response);
					}
				}
			}
		} else {

			response.sendRedirect(request.getParameter("from"));
		}

	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		UserBean userBean = new UserBean();

		boolean isValid = UserValidator.validateSignup(request, userBean);
		if (isValid) {
			DAOFactory javabase = DAOFactory.getInstance("javabase.jdbc");
			UserDAO userDAO = javabase.getUserDAO();
			userDAO.create(userBean);
			request.getSession().setAttribute("loggedin", true);
		}
		request.setAttribute("isValid", isValid);
		request.getSession().setAttribute("userbean", userBean);
		doGet(request, response);
	}

}
