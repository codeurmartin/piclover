package mrtn.piclover.servlets;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mrtn.piclover.beans.ImageBean;
import mrtn.piclover.services.ImagePathService;
import mrtn.piclover.services.Uploader;

@MultipartConfig()
public class MainPageServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final int maxFileSize = 1200000;

	public MainPageServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		List<ImageBean> beanList = ImagePathService.getImageBeans("public");
		request.getSession().setAttribute("imagebeans", beanList);

		request.getRequestDispatcher("/WEB-INF/jspfiles/MainPage.jsp").forward(request, response);

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		// check if the file Part is correctly selected and if it exceeds the
		// maxFileSize catches the exception
		try {
			if (request.getPart("file") != null) {
				request.setAttribute("uploaded", Uploader.uploadImagePublic(request, maxFileSize));
			}
		} catch (IllegalStateException e) {

		}
		doGet(request, response);
	}

}
