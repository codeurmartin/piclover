package mrtn.piclover.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mrtn.piclover.beans.UserBean;
import mrtn.piclover.cookies.CookieUtil;
import mrtn.piclover.dao.DAOFactory;
import mrtn.piclover.dao.UserDAO;

/**
 * Servlet implementation class LogInServlet
 */

public class LogInServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public LogInServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		request.getRequestDispatcher("/WEB-INF/jspfiles/LogIn.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String nickName = request.getParameter("nickname");
		String password = request.getParameter("password");
		String from = request.getParameter("from");

		if (request.getParameter("return") == null) {
			if (nickName.length() > 0 && password.length() > 0) {
				DAOFactory javabase = DAOFactory.getInstance("javabase.jdbc");
				UserDAO userDAO = javabase.getUserDAO();
				UserBean userbean = userDAO.find(nickName, password);
				if (userbean != null) {

					request.getSession().setAttribute("userbean", userbean);
					request.getSession().setAttribute("loggedin", true);
					/*
					 * if checkbox remember is marked, a cookie will be created
					 */
					if (request.getParameter("remember") != null) {
						CookieUtil.createCookie(response, userbean);
					}
					if (request.getSession().getAttribute("from") != null) {
						response.sendRedirect(request.getParameter("from"));
					} else {
						response.sendRedirect("MainPageServlet");
						// request.getRequestDispatcher("MainPageServlet").forward(request,
						// response);
					}
				} else {
					request.setAttribute("invalidinput", "user and/or password incorrect");
					doGet(request, response);
				}
			} else {
				request.setAttribute("invalidinput", "user and/or password incorrect");
				doGet(request, response);
			}
		} else {

			System.out.println("from in login: " + request.getParameter("from"));
			if (from.equals("SecretItServlet") || from.equals("SecretKittenServlet") || from == "") {

				response.sendRedirect("MainPageServlet");
			} else {
				response.sendRedirect(request.getParameter("from"));
			}

		}

	}

}
