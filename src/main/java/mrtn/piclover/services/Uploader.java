package mrtn.piclover.services;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.Part;

import mrtn.piclover.beans.ImageBean;
import mrtn.piclover.dao.DAOFactory;
import mrtn.piclover.dao.ImageDAO;

public class Uploader {
	// static class to upload files to make the image uploading centralised;

	private Uploader() {
	}

	public static String uploadImagePublic(HttpServletRequest request, int bytes) throws IOException, ServletException {

		DAOFactory javabase = DAOFactory.getInstance("javabase.jdbc");
		ImageDAO imageDAO = javabase.getImageDAO();
		int result = imageDAO.returnLastImageId() + 1;

		Part filePart = request.getPart("file");
		if (filePart.getSize() != 0) {
			if (filePart.getSize() < bytes) {
				Path path = Paths.get(System.getenv("UPLOAD_LOCATION_PUBLIC") + "image_" + result + ".jpg");
				try (InputStream input = filePart.getInputStream()) {
					Files.copy(input, path);
					imageDAO.createPublicImage(new ImageBean());
					return "succesfully uploaded";
				} catch (IOException e) {
					e.printStackTrace();
				}
			} else {
				return "file is too big, cannot be bigger then: " + (((double) bytes / 1000000)) + " MB";
			}
		} else {
			return "no file selected";
		}
		return null;

	}

}
