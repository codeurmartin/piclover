package mrtn.piclover.services;

import java.util.ArrayList;
import java.util.List;

import mrtn.piclover.beans.ImageBean;
import mrtn.piclover.dao.DAOFactory;
import mrtn.piclover.dao.ImageDAO;

public class ImagePathService {

	private static final String publicImagePath = "/images/public/";
	private static final String kittensImagePath = "/images/secret/kittens/";
	private static final String itImagePath = "/images/secret/it/";

	private ImagePathService() {
	};

	public static List<ImageBean> getImageBeans(String source) {

		DAOFactory javabase = DAOFactory.getInstance("javabase.jdbc");
		ImageDAO imageDAO = javabase.getImageDAO();
		List<ImageBean> beanList = new ArrayList<>();
		if (source.equals("public")) {
			List<String> imageNumbersList = imageDAO.getRandomImagesPublic();
			for (String s : imageNumbersList) {
				ImageBean imageBean = new ImageBean();
				imageBean.setPath(publicImagePath + "image_" + s + ".jpg");
				beanList.add(imageBean);
			}
			return beanList;
		} else if (source.equals("kittens")) {
			List<String> imageNumbersList = imageDAO.getRandomImagesKittens();
			for (String s : imageNumbersList) {
				ImageBean imageBean = new ImageBean();
				imageBean.setPath(kittensImagePath + "image_" + s + ".jpg");
				beanList.add(imageBean);

			}
		} else if (source.equals("it")) {
			List<String> imageNumbersList = imageDAO.getRandomImagesIt();
			for (String s : imageNumbersList) {
				ImageBean imageBean = new ImageBean();
				imageBean.setPath(itImagePath + "image_" + s + ".jpg");
				beanList.add(imageBean);

			}
		}
		return beanList;

	}

}
