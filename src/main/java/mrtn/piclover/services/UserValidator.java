package mrtn.piclover.services;

import javax.servlet.http.HttpServletRequest;

import mrtn.piclover.beans.UserBean;
import mrtn.piclover.dao.DAOFactory;
import mrtn.piclover.dao.UserDAO;

public class UserValidator {

	private UserValidator() {
	}

	public static boolean validatePassword(HttpServletRequest request, UserBean user) {
		String password = request.getParameter("password");
		String retypePassword = request.getParameter("retypepassword");
		if (password.length() > 0) {
			if (password.length() < 6 || !password.equals(retypePassword)) {
				return false;
			} else {
				user.setPassword(password);
				return true;

			}
		}
		return false;
	}

	public static boolean validateChangeUser(HttpServletRequest request, UserBean userBean) {
		boolean isValid = true;

		String firstName = request.getParameter("firstname");
		String lastName = request.getParameter("lastname");
		String street = request.getParameter("street");
		String postCode = request.getParameter("postcode");

		if ((firstName != null) && (firstName.length() > 0)) {
			userBean.setFirstname(firstName);
		} else {
			isValid = false;

		}
		if (lastName != null && lastName.length() > 0) {
			userBean.setLastname(lastName);
		} else {
			isValid = false;
		}
		if (street != null && street.length() > 0) {
			userBean.setStreet(street);
		} else {
			isValid = false;

		}
		if (postCode != null && postCode.length() > 0) {
			try {
				int postcode = Integer.parseInt(postCode);
				userBean.setPostcode(postcode);
			} catch (NumberFormatException nfe) {
				isValid = false;
			}
		} else {
			isValid = false;
		}

		return isValid;

	}

	public static boolean validateSignup(HttpServletRequest request, UserBean userBean) {
		boolean isValid = true;

		String firstName = request.getParameter("firstname");
		String lastName = request.getParameter("lastname");
		String street = request.getParameter("street");
		String nickName = request.getParameter("nickname");
		String postCode = request.getParameter("postcode");
		String password = request.getParameter("password");
		String retypePassword = request.getParameter("retypepassword");

		if (firstName != null) {
			userBean.setFirstname(firstName);
		} else {
			isValid = false;

		}
		if (lastName != null) {
			userBean.setLastname(lastName);
		} else {
			isValid = false;

		}
		if (street != null) {
			userBean.setStreet(street);
		} else {
			isValid = false;

		}
		if (nickName != null) {
			/*
			 * Check if username exists in Database, if so, it is not a valid
			 * nickname
			 */
			DAOFactory javabase = DAOFactory.getInstance("javabase.jdbc");
			UserDAO userDAO = javabase.getUserDAO();
			if (!userDAO.existNickname(nickName)) {
				userBean.setNickname(nickName);
			}
		} else {
			isValid = false;

		}

		if (postCode != null) {
			try {
				int postcode = Integer.parseInt(postCode);
				userBean.setPostcode(postcode);
			} catch (NumberFormatException nfe) {
				isValid = false;
			}
		} else {
			isValid = false;

		}
		if (password != null && retypePassword != null) {

			if (password.length() > 6 || retypePassword.length() > 6) {

				if (password.equals(retypePassword)) {
					userBean.setPassword(password);
				} else {
					isValid = false;
				}
			} else {
				isValid = false;
			}

		} else {
			isValid = false;

		}
		return isValid;
	}

}
