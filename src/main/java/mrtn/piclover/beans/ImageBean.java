package mrtn.piclover.beans;

import java.io.Serializable;

public class ImageBean implements Serializable {

	private Integer id;
	private String path;

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

}
