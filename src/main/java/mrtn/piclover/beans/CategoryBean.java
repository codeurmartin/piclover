package mrtn.piclover.beans;

//@Entity(name = "categories")
public class CategoryBean {
	// @Id
	private int idcategories;

	private String category;

	public int getIdcategories() {
		return idcategories;
	}

	public void setIdcategories(int idcategories) {
		this.idcategories = idcategories;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

}
