package mrtn.piclover.beans;

import java.io.Serializable;

public class UserBean implements Serializable {

	// Constants
	// ----------------------------------------------------------------------------------

	private static final long serialVersionUID = 1L;

	// Properties
	// ---------------------------------------------------------------------------------

	private Long id;
	private String street;
	private String password;
	private String firstname;
	private String lastname;
	private String nickname;
	private int postcode;

	// Getters/setters
	// ----------------------------------------------------------------------------

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public String getNickname() {
		return nickname;
	}

	public void setNickname(String nickName) {
		this.nickname = nickName;
	}

	public int getPostcode() {
		return postcode;
	}

	public void setPostcode(int postCode) {
		this.postcode = postCode;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	// Object overrides
	// ---------------------------------------------------------------------------

	/**
	 * The user ID is unique for each User. So this should compare User by ID
	 * only.
	 *
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object other) {
		return (other instanceof UserBean) && (id != null) ? id.equals(((UserBean) other).id) : (other == this);
	}

	/**
	 * The user ID is unique for each User. So User with same ID should return
	 * same hashcode.
	 *
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		return (id != null) ? (this.getClass().hashCode() + id.hashCode()) : super.hashCode();
	}

	/**
	 * Returns the String representation of this User. Not required, it just
	 * pleases reading logs.
	 *
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return String.format("User[id=%d,street=%s,nickname=%s,firstname=%s,lastname=%s]", id, street, nickname,
				firstname, lastname);
	}

}
