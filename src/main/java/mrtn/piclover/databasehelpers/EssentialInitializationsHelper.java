package mrtn.piclover.databasehelpers;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

/*
 * Purpose of this class is to initialize The imageCategories if needed;
 */
public class EssentialInitializationsHelper {
	private final static String publicCategory = "public";
	private final static String secretKittensCategory = "secret_kittens";
	private final static String secretItCategory = "secret_it";

	public static void createCategories() {
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("dbConn");
		EntityManager em = emf.createEntityManager();
		EntityTransaction tx = em.getTransaction();

		tx.begin();

		tx.commit();
		em.close();
		emf.close();

	}

	public static void main(String[] args) {
		createCategories();
	}
}
