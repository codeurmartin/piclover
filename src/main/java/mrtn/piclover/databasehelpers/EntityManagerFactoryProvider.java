package mrtn.piclover.databasehelpers;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class EntityManagerFactoryProvider {
	private static EntityManagerFactory emf;

	public static EntityManagerFactory getEntityManagerFactory() {
		if (emf != null) {
			return emf;
		} else {
			emf = Persistence.createEntityManagerFactory("dbConn");
			return emf;
		}
	}

}
