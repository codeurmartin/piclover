package mrtn.piclover.dao;

import static mrtn.piclover.dao.DAOUtil.prepareStatement;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import mrtn.exceptions.DAOException;
import mrtn.piclover.beans.UserBean;

public class UserDAOJDBC implements UserDAO {

	// Constants
	// ----------------------------------------------------------------------------------

	private static final String SQL_FIND_BY_ID = "SELECT id, nickname, street, postcode, firstname, lastname FROM User WHERE id = ?";
	private static final String SQL_FIND_BY_NICKNAME_AND_PASSWORD = "SELECT id, nickname, street, postcode, firstname, lastname FROM User WHERE nickname = ? AND password = MD5(?)";
	private static final String SQL_FIND_BY_NICKNAME = " SELECT id, nickname, street, postcode, firstname, lastname FROM user WHERE nickname = ?";
	private static final String SQL_LIST_ORDER_BY_ID = "SELECT id, nickname, street, postcode, firstname, lastname FROM User ORDER BY id";
	private static final String SQL_INSERT = "INSERT INTO User (nickname, street, postcode, password, firstname, lastname ) VALUES (?, ?, ?, MD5(?), ?, ?)";
	private static final String SQL_UPDATE = "UPDATE User SET street = ?, postcode = ?, firstname = ?, lastname = ? WHERE id = ?";
	private static final String SQL_DELETE = "DELETE FROM User WHERE id = ?";
	private static final String SQL_EXIST_NICKNAME = "SELECT id FROM User WHERE nickname = ?";
	private static final String SQL_CHANGE_PASSWORD = "UPDATE User SET password = MD5(?) WHERE id = ?";

	// Vars
	// ---------------------------------------------------------------------------------------

	private DAOFactory daoFactory;

	// Constructors
	// -------------------------------------------------------------------------------

	/**
	 * Construct an User DAO for the given DAOFactory. Package private so that
	 * it can be constructed inside the DAO package only.
	 *
	 * @param daoFactory
	 *            The DAOFactory to construct this User DAO for.
	 */
	UserDAOJDBC(DAOFactory daoFactory) {
		this.daoFactory = daoFactory;
	}

	// Actions
	// ------------------------------------------------------------------------------------

	@Override
	public UserBean find(Long id) throws DAOException {
		return findbean(SQL_FIND_BY_ID, id);
	}

	@Override
	public UserBean find(String nickName) throws DAOException {
		return findbean(SQL_FIND_BY_NICKNAME, nickName);
	}

	@Override
	public UserBean find(String email, String password) throws DAOException {
		return findbean(SQL_FIND_BY_NICKNAME_AND_PASSWORD, email, password);
	}

	/**
	 * Returns the UserBean from the database matching the given SQL query with
	 * the given values.
	 *
	 * @param sql
	 *            The SQL query to be executed in the database.
	 * @param values
	 *            The PreparedStatement values to be set.
	 * @return The UserBean from the database matching the given SQL query with
	 *         the given values.
	 * @throws DAOException
	 *             If something fails at database level.
	 */
	private UserBean findbean(String sql, Object... values) throws DAOException {
		UserBean UserBean = null;

		try (Connection connection = daoFactory.getConnection();
				PreparedStatement statement = prepareStatement(connection, sql, false, values);
				ResultSet resultSet = statement.executeQuery();) {
			if (resultSet.next()) {
				UserBean = map(resultSet);
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		}

		return UserBean;
	}

	@Override
	public List<UserBean> list() throws DAOException {
		List<UserBean> UserBeans = new ArrayList<>();

		try (Connection connection = daoFactory.getConnection();
				PreparedStatement statement = connection.prepareStatement(SQL_LIST_ORDER_BY_ID);
				ResultSet resultSet = statement.executeQuery();) {
			while (resultSet.next()) {
				UserBeans.add(map(resultSet));
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		}

		return UserBeans;
	}

	@Override
	public void create(UserBean UserBean) throws IllegalArgumentException, DAOException {
		if (UserBean.getId() != null) {
			throw new IllegalArgumentException("User is already created, the User ID is not null.");
		}

		Object[] values = { UserBean.getNickname(), UserBean.getStreet(), UserBean.getPostcode(),
				UserBean.getPassword(), UserBean.getFirstname(), UserBean.getLastname() };

		try (Connection connection = daoFactory.getConnection();
				PreparedStatement statement = prepareStatement(connection, SQL_INSERT, true, values);) {
			int affectedRows = statement.executeUpdate();
			if (affectedRows == 0) {
				throw new DAOException("Creating User failed, no rows affected.");
			}

			try (ResultSet generatedKeys = statement.getGeneratedKeys()) {
				if (generatedKeys.next()) {
					UserBean.setId(generatedKeys.getLong(1));
				} else {
					throw new DAOException("Creating User failed, no generated key obtained.");
				}
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		}
	}

	@Override
	public void update(UserBean UserBean) throws DAOException {
		if (UserBean.getId() == null) {
			throw new IllegalArgumentException("User is not created yet, the User ID is null.");
		}

		Object[] values = { UserBean.getStreet(), UserBean.getPostcode(), UserBean.getFirstname(),
				UserBean.getLastname(), UserBean.getId() };

		try (Connection connection = daoFactory.getConnection();
				PreparedStatement statement = prepareStatement(connection, SQL_UPDATE, false, values);) {
			int affectedRows = statement.executeUpdate();
			if (affectedRows == 0) {
				throw new DAOException("Updating Userfailed, no rows affected.");
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		}
	}

	@Override
	public void delete(UserBean UserBean) throws DAOException {
		Object[] values = { UserBean.getId() };

		try (Connection connection = daoFactory.getConnection();
				PreparedStatement statement = prepareStatement(connection, SQL_DELETE, false, values);) {
			int affectedRows = statement.executeUpdate();
			if (affectedRows == 0) {
				throw new DAOException("Deleting User failed, no rows affected.");
			} else {
				UserBean.setId(null);
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		}
	}

	@Override
	public boolean existNickname(String nickname) throws DAOException {
		Object[] values = { nickname };

		boolean exist = false;

		try (Connection connection = daoFactory.getConnection();
				PreparedStatement statement = prepareStatement(connection, SQL_EXIST_NICKNAME, false, values);
				ResultSet resultSet = statement.executeQuery();) {
			exist = resultSet.next();
		} catch (SQLException e) {
			throw new DAOException(e);
		}

		return exist;
	}

	@Override
	public void changePassword(UserBean UserBean) throws DAOException {
		if (UserBean.getId() == null) {
			throw new IllegalArgumentException("User is not created yet, the User ID is null.");
		}

		Object[] values = { UserBean.getPassword(), UserBean.getId() };

		try (Connection connection = daoFactory.getConnection();
				PreparedStatement statement = prepareStatement(connection, SQL_CHANGE_PASSWORD, false, values);) {
			int affectedRows = statement.executeUpdate();
			if (affectedRows == 0) {
				throw new DAOException("Changing password failed, no rows affected.");
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		}
	}

	// Helpers
	// ------------------------------------------------------------------------------------

	/**
	 * Map the current row of the given ResultSet to an UserBean.
	 *
	 * @param resultSet
	 *            The ResultSet of which the current row is to be mapped to an
	 *            UserBean.
	 * @return The mapped UserBean from the current row of the given ResultSet.
	 * @throws SQLException
	 *             If something fails at database level.
	 */
	private static UserBean map(ResultSet resultSet) throws SQLException {
		UserBean UserBean = new UserBean();
		UserBean.setId(resultSet.getLong("id"));
		UserBean.setNickname(resultSet.getString("nickname"));
		UserBean.setStreet(resultSet.getString("street"));
		UserBean.setPostcode(resultSet.getInt("postcode"));
		UserBean.setFirstname(resultSet.getString("firstname"));
		UserBean.setLastname(resultSet.getString("lastname"));
		return UserBean;
	}

}