package mrtn.piclover.dao;

import java.util.List;

import mrtn.exceptions.DAOException;
import mrtn.piclover.beans.ImageBean;

public interface ImageDAO {

	/**
	 * Get the last imageId to assign the correct number to a newly created
	 * image in the corresponding category
	 *
	 * @return
	 */
	int returnLastImageId() throws DAOException;

	int returnLastImageIdPublic() throws DAOException;

	ImageBean getImageById() throws DAOException;

	public void createPublicImage(ImageBean image) throws IllegalArgumentException, DAOException;

	public List<String> getRandomImagesPublic() throws DAOException;

	public List<String> getRandomImagesKittens() throws DAOException;

	public List<String> getRandomImagesIt() throws DAOException;

}
