package mrtn.piclover.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import mrtn.exceptions.DAOException;
import mrtn.piclover.beans.ImageBean;

public class ImageDAOJDBC implements ImageDAO {

	private static final String SQL_CREATE_PUBLIC_IMAGE = "insert into picloverdb.images values(null, \"public\")";
	private static final String SQL_FIND_LAST_ID = "select idimages from picloverdb.images where idimages=(select max(idimages) from picloverdb.images)";
	private static final String SQL_FIND_LAST_ID_PUBLIC = "select idimages from picloverdb.images where idimages =(select max(idimages) from picloverdb.images where category "
			+ "= \"public\")";
	private static final String SQL_FIND_LAST_ID_KITTENS = "select idimages from picloverdb.images where idimages =(select max(idimages) from picloverdb.images where category "
			+ "= \"secret_kittens\")";
	private static final String SQL_FIND_LAST_ID_IT = "select idimages from picloverdb.images where idimages =(select max(idimages) from picloverdb.images where category "
			+ "= \"secret_it\")";
	private static final String SQL_GET_RANDOM_IMAGES_PUBLIC = "select idimages from images where category=\"public\" order by RAND() limit 6";
	private static final String SQL_GET_RANDOM_IMAGES_KITTENS = "select idimages from images where category=\"secret_kittens\" order by RAND() limit 6";
	private static final String SQL_GET_RANDOM_IMAGES_IT = "select idimages from images where category=\"secret_it\" order by RAND() limit 6";

	private DAOFactory daoFactory;

	ImageDAOJDBC(DAOFactory daoFactory) {
		this.daoFactory = daoFactory;
	}

	@Override
	public int returnLastImageId() throws DAOException {
		int result = 0;
		try (Connection connection = daoFactory.getConnection();
				PreparedStatement statement = connection.prepareStatement(SQL_FIND_LAST_ID);
				ResultSet resultSet = statement.executeQuery();) {
			while (resultSet.next()) {
				result = resultSet.getInt("idimages");
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		}
		return result;
	}

	@Override
	public ImageBean getImageById() throws DAOException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int returnLastImageIdPublic() throws DAOException {
		int result = 0;
		try (Connection connection = daoFactory.getConnection();
				PreparedStatement statement = connection.prepareStatement(SQL_FIND_LAST_ID_PUBLIC);
				ResultSet resultSet = statement.executeQuery();) {
			while (resultSet.next()) {
				result = resultSet.getInt("idimages");
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		}
		return result;
	}

	@Override
	public void createPublicImage(ImageBean image) throws IllegalArgumentException, DAOException {
		if (image.getId() != null) {
			throw new IllegalArgumentException("Image is already assigned, image is not null");
		}

		try (Connection connection = daoFactory.getConnection();
				PreparedStatement statement = connection.prepareStatement(SQL_CREATE_PUBLIC_IMAGE)) {
			int affectedRows = statement.executeUpdate();
			if (affectedRows == 0) {
				throw new DAOException("Creating Image failed, no rows affected.");
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		}

	}

	@Override
	public List<String> getRandomImagesPublic() throws DAOException {
		List<String> imageNumbers = new ArrayList<>();
		try (Connection connection = daoFactory.getConnection();
				PreparedStatement statement = connection.prepareStatement(SQL_GET_RANDOM_IMAGES_PUBLIC)) {
			ResultSet rs = statement.executeQuery();

			while (rs.next()) {
				imageNumbers.add(String.valueOf(rs.getInt("idimages")));
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return imageNumbers;
	}

	@Override
	public List<String> getRandomImagesKittens() throws DAOException {
		List<String> imageNumbers = new ArrayList<>();
		try (Connection connection = daoFactory.getConnection();
				PreparedStatement statement = connection.prepareStatement(SQL_GET_RANDOM_IMAGES_KITTENS)) {
			ResultSet rs = statement.executeQuery();

			while (rs.next()) {
				imageNumbers.add(String.valueOf(rs.getInt("idimages")));
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return imageNumbers;
	}

	@Override
	public List<String> getRandomImagesIt() throws DAOException {

		List<String> imageNumbers = new ArrayList<>();
		try (Connection connection = daoFactory.getConnection();
				PreparedStatement statement = connection.prepareStatement(SQL_GET_RANDOM_IMAGES_IT)) {
			ResultSet rs = statement.executeQuery();

			while (rs.next()) {
				imageNumbers.add(String.valueOf(rs.getInt("idimages")));
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return imageNumbers;
	}

}
