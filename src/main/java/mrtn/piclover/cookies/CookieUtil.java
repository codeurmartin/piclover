package mrtn.piclover.cookies;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mrtn.piclover.beans.UserBean;
import mrtn.piclover.dao.DAOFactory;
import mrtn.piclover.dao.UserDAO;

public class CookieUtil {
	private static final int maxAgeInSeconds = 60 * 60; // 1 hour

	private CookieUtil() {
	}

	public static void createCookie(HttpServletResponse response, UserBean userbean) {
		Cookie cookie = new Cookie("loggedin", userbean.getNickname());
		cookie.setMaxAge(maxAgeInSeconds);
		cookie.setPath("/");
		response.addCookie(cookie);
	}

	public static UserBean getUserFromCookie(HttpServletRequest request) {
		Cookie[] cookies = request.getCookies();
		UserBean userbean = null;
		if (cookies != null) {
			for (Cookie k : cookies) {
				if (k.getName().equals("loggedin")) {
					DAOFactory javabase = DAOFactory.getInstance("javabase.jdbc");
					UserDAO userDAO = javabase.getUserDAO();
					userbean = userDAO.find(k.getValue());
				}
			}
		}
		return userbean;
	}

	public static void deleteUserCookie(HttpServletRequest request, HttpServletResponse response, UserBean userbean) {
		Cookie[] cookies = request.getCookies();
		if (cookies != null) {
			for (Cookie k : cookies) {
				if (k.getName().equals("loggedin")) {
					if (k.getValue().equals(userbean.getNickname())) {
						k.setPath("/");
						k.setValue(null);
						k.setMaxAge(0);
						response.addCookie(k);
					}
				}
			}
		}

	}
}
